
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}



function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();
	var aktivnost = $("#kreirajAktivnost").val();
	var spol = $("#kreirajSpol").val();
	console.log(aktivnost);
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
      	$('#izhodObvestilo').empty();
		$("#izhodObvestilo").append("<div style='visibility:visible;background-color: #efbc02;border-radius: 50px; float: right; width: 50%; margin-top: 30px;text-align: center; height: 60px;padding-top: 15px;animation-name: exampleWrong;animation-duration: 1s;'>Prosim vnesite zahtevane podatke!</div>");
		
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{
		                key: "ehrId", value: ehrId },{
		                key: "activity", value: aktivnost},{
		                key: "genre", value: spol
		            }]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	$('#izhodObvestilo').empty();
		                    $("#izhodObvestilo").append("<div style='visibility:visible;background-color: #58b20e;color: white;text-align: center;border-radius: 50px; float: right; width: 50%; margin-top: 30px; height: 60px;padding-top: 15px;animation-name: exampleCorrect;animation-duration: 1s;'>Kreiran EHR: " +ehrId+"</div>");
		                }
		            },
		            error: function(err) {
		            	$('#izhodObvestilo').empty();
		            	$("#izhodObvestilo").append("<div style='visibility:visible;background-color: #efbc02;border-radius: 50px;text-align: center; float: right; width: 50%; margin-top: 30px; height: 60px;padding-top: 15px;animation-name: exampleWrong;animation-duration: 1s;'>Neustrezni podatki</div>");
		            }
		        });
		    }
		});
	}
}
var partyAdditional = [];
function preberiEHRid() {
	BMI	= -1;
	sessionId = getSessionId();
	preberiMeritveVitalnihZnakov();
	$("#statistika").css({"visibility": "hidden"});
	var ehrId = $("#uporabniskoIme").val();
	var spol;
	stalniEHR = ehrId;
	klici=0;
	if (!ehrId || ehrId.trim().length == 0) {
		$('#izhodObvestilo').empty();
		$("#izhodObvestilo").append("<div style='visibility:visible;background-color: #efbc02;border-radius: 50px; float: right; width: 50%; margin-top: 30px;text-align: center; height: 60px;padding-top: 15px;animation-name: exampleWrong;animation-duration: 1s;'>Prosim vnesite EHR ID!</div>");
	} else {
		$('#izhodObvestilo').empty();
		vrniZnake();
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				var xnumber = Math.floor((Math.random() * 95) + 1);
				var partyAdditional = party.partyAdditionalInfo;
				var vrednost;
				for(var i=0; i<partyAdditional.length; i++){
					if(partyAdditional[i]["key"] === "activity")
						vrednost = partyAdditional[i]["value"];
				}
				for(var i=0; i<partyAdditional.length; i++){
					if(partyAdditional[i]["key"] === "genre")
						spol = partyAdditional[i]["value"];
				}
				if(spol==0)
					spol="men";
				else	
					spol="women";
				BMIAktivnost = vrednost;
				$("#showName").html(party.firstNames);
				$("#showSurname").html(party.lastNames);
				$("#showBitrh").html(party.dateOfBirth);
				if(vrednost==0)
					$("#showActivity").html("Manj kot 2 uri na teden");
				else if(vrednost==1)
					$("#showActivity").html("Več kot 2 in manj kot 5 ur na teden");
				else if(vrednost==2)
					$("#showActivity").html("Več kot 5 ur na teden");
				else $("#showActivity").html("Ni podatka");
				//$('#showPicture').empty();
				/*$("#showPicture").append(" \
                        <div class='media-body room' style='cursor: pointer;'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle' src='https://randomuser.me/api/portraits/"+spol+"/"+xnumber+".jpg'/> \
                                </a> \
                        </div>");*/

			},
			error: function(err) {
				$("#showName").html("NULL");
				$("#showSurname").html("NULL");
				$("#showBitrh").html("NULL");
			}
		});
	}
}
var BMIAktivnost;

function dodajVitalneMeritve() {
	sessionId = getSessionId();
	var ehrId = $("#vnesiEHR").val();
	var datumInUra = $("#vnesiDatum").val();
	var telesnaVisina = $("#vnesiVisino").val();
	var telesnaTeza = $("#vnesiTezo").val();
	var telesnaTemperatura = $("#vnesiTemperaturo").val();
	var sistolicniKrvniTlak = $("#vnesiSisTlak").val();
	var diastolicniKrvniTlak = $("#vnesiDisTlak").val();
	var nasicenostKrviSKisikom = $("#vnesiKisik").val();
	//var nivoSladkorjaVKrvi = $("#vnesiSladkor").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajanjeMeritev").html("<span class='obvestilo'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		    //"vital_signs/blood_pressure/any_event/bloodSugar": nivoSladkorjaVKrvi
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "SAM"
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajanjeMeritev").html(
              "Meritve so bile uspešno dodane!");
		    },
		    error: function(err) {
		    	$("#dodajanjeMeritev").html(
            "Meritev ni bilo mogoče dodati!");
		    }
		});
	}
}
var teza;
var visina;
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var ehrId = $("#uporabniskoIme").val();
	stalniEHR = ehrId;
	if (!ehrId || ehrId.trim().length == 0) {
	//	$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
     // "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
				$.ajax({
			  		    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var trenutnaTeza = res[0].weight;
						    	teza = trenutnaTeza;
						        //for (var i in res) {
						        //    trenutnaTeza = res[i].telesnaTeza;
						        //}
						        $("#showWeight").html(trenutnaTeza);
					    	} else {
					    		teza = 0;
					    		$("#showWeight").html("Ni podatkov");
					    	}
					    },
					    error: function() {
					    	$("#showWeight").html("Napaka");
					    }
					});
				
				$.ajax({
			  		    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var trenutnaVisina = res[0].height;
						    	visina = trenutnaVisina/100;
						        //for (var i in res) {
						        //    trenutnaTeza = res[i].telesnaTeza;
						        //}
						        $("#showHeight").html(trenutnaVisina);
					    	} else {
					    		visina=0;
					    		$("#showHeight").html("Ni podatkov");
					    	}
					    },
					    error: function() {
					    	$("#showHeight").html("Napaka");
					    }
					});
	}
}




// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
/*
function vrniData(){
	var polja = [
         ['Datum', 'Sistolični tlak', 'Distolični tlak'],
         ['2004/05',  165,      938],
         ['2005/06',  135,      1120],
         ['2006/07',  157,      1167],
         ['2007/08',  139,      1110],
         ['2005/06',  135,      1120],
         ['2006/07',  157,      1167],
         ['2007/08',  139,      1110],
         ['2008/09',  136,      691]
      ];
    polja.push(['2016/2017', Math.floor(Math.random()*1000), 1000]);
    return polja;
}
*/

var klici=0;
function izris(){
	klik1=0;
	klik2=0;
	$("#polje1").css({"display": "none"});
	$("#polje2").css({"display": "none"});
	if(stalniEHR.length!=0){
	klici++;
		if(klici%2==0){
			$("#statistika").css({"visibility": "hidden"});
		}
		else{	
			$("#statistika").css({"visibility": "visible"});
		}	
	if(teza!=0 && visina!=0){
		var ITM = teza/(visina*visina);
		BMI = ITM
		$("#showITM").html("  "+parseFloat(ITM).toFixed(2));
	}
	else{
		BMI	= -1;
		$("#showITM").html(" /");
	}
	obvestila();
	izris2();
	izris1();
	}
}
var BMI;

function izris1(){
        var stev = znaki; 
        if(znakiSisPovp>0){
        	if(znakiSisPovp>140 && (znakiDisPovp >=60 && znakiDisPovp <=90) )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak večji od mej normale, ki so od 110 do 140 mmHg, vaš diastnolični tlak pa ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if(znakiSisPovp>140 && znakiDisPovp<60 )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak večji od mej normale, ki so od 110 do 140 mmHg, vaš diastnolični tlak pa je prenizek in ne ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if(znakiSisPovp>140 && znakiDisPovp>90 )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak večji od mej normale, ki so od 110 do 140 mmHg, ravno tako je previsok tudi vaš diastnolični tlak, ki ne ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if(znakiSisPovp<110 && (znakiDisPovp >=60 && znakiDisPovp <=90) )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak manjši od mej normale, ki so od 110 do 140 mmHg, vaš diastnolični tlak pa ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if(znakiSisPovp<110 && znakiDisPovp<60 )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak manjši od mej normale, ki so od 110 do 140 mmHg, ravno tako pa je prenizek tudi vaš diastnolični tlak in ne ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if(znakiSisPovp<110 && znakiDisPovp>90 )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak manji od mej normale, ki so od 110 do 140 mmHg, previsok pa je vaš diastnolični tlak, ki ne ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if((znakiSisPovp >=110 && znakiSisPovp <=140) && (znakiDisPovp >=60 && znakiDisPovp <=90) )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak v mejah normale, ki so od 110 do 140 mmHg. Tudi vaš diastnolični tlak je znoraj mej priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg.");
        	else if((znakiSisPovp >=110 && znakiSisPovp <=140) && znakiDisPovp<60 )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak v mejah normale, ki so od 110 do 140 mmHg, prenizek pa je vaš diastnolični tlak, ki ne ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if((znakiSisPovp >=110 && znakiSisPovp <=140) && znakiDisPovp>90 )
        		$("#showAnalize1").html("Po pregledu vaših meritev krvnega tlaka, smo ugotovili, da je vaš sistolični krvni tlak v mejah normale, ki so od 110 do 140 mmHg, previsok pa je vaš diastnolični tlak, ki ne ustreza mejam priporočljivim za diastolični tlak, ki so od 60 do 90 mmHg. Svetujemo vam obisk pri osebnem zdravniku.");
        }
        else{;
        	$("#showAnalize2").html("Ni dovolj meritev za analizo");
        }
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawVisualization);
          function drawVisualization() {
            // Some raw data (not necessarily accurate)
            var data = google.visualization.arrayToDataTable(stev);
    
        var options = {
          title : 'Prikaz meritev tlaka',
          titleTextStyle: {fontSize: 30},
          chartArea: {width: '100px', height: '100px'},
          vAxis: {title: ''},
          hAxis: {title: 'Datum',
          		  titleTextStyle: {fontSize: 20},
        		},
		  backgroundColor: '#d1f4ab',
          seriesType: 'bars',
          series: {5: {type: 'line'}},
          point: {visible: true}
        };
    
        var chart = new google.visualization.ComboChart(document.getElementById('graf'));
        chart.draw(data, options);

		//izrisi1();
      }
}

function izris2(){
        var stev = znakiTemp; 
        if(znakiTempPovp>0){
        	if(znakiTempPovp>37.2)
        		$("#showAnalize2").html("Po pregledu vaših meritev temperature je vaša telesna temperatura, glede na dobljeno povprečje posameznih meritev, nad normalno mejo, ki znaša 37,2 °C. Temu pojavu pravimo temperaturna porast oz. hipertermija. Zavedati se morate, da je lahko temperatura nad 40 °C lahko že smrtno nevarna in lahko vodi tudi do invalidnosti. Svetujemo vam obisk pri osebnem zdravniku.");
        	else if(znakiTempPovp<35.8)
        		$("#showAnalize2").html("Po pregledu vaših meritev temperature je vaša telesna temperatura, glede na dobljeno povprečje posameznih meritev, pod normalno mejo, ki znaša 35,8 °C. Temu pojavu pravimo temperaturni padec oz. hipotermija. Zavedati se morate, da je lahko temperatura pod 27 °C lahko že smrtno nevarna. Svetujemo vam obisk pri osebnem zdravniku.");
			else 
				$("#showAnalize2").html("Po pregledu vaših meritev temperature je vaša telesna temperatura, glede na dobljeno povprečje posameznih meritev, v mejah normale, ki so od 35,8 do 37,2 °C.");
        }
        else{
        	$("#showAnalize2").html("Ni dovolj meritev za analizo");
        }
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawVisualization);
          function drawVisualization() {
            // Some raw data (not necessarily accurate)
            var data = google.visualization.arrayToDataTable(znakiTemp);
    
        var options = {
          title : 'Prikaz meritev temperature',
          titleTextStyle: {fontSize: 30},
          chartArea: {width: '100px', height: '100px'},
          vAxis: {title: ''},
          hAxis: {title: 'Datum',
          		  titleTextStyle: {fontSize: 20},
        		},
		  backgroundColor: '#d1f4ab',
          seriesType: 'bars',
          series: {5: {type: 'line'}},
          point: {visible: true}
        };
    
        var chart = new google.visualization.ComboChart(document.getElementById('graf1'));
        chart.draw(data, options);

      }
}



var stalniEHR;
var znaki;
var znakiDisPovp=0;
var znakiSisPovp=0;
var znakiTemp;
var znakiTempPovp=0;
function vrniZnake() {
	sessionId = getSessionId();
	znaki =[['Datum', 'Diastolični tlak [mm Hg]', 'Sistolični tlak [mm Hg]']];
	znakiTemp =[['Datum', 'Temperatura [°C]']];
	if (!stalniEHR || stalniEHR.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} 
	else {
		$.ajax({
  			    url: baseUrl + "/view/" + stalniEHR + "/" + "blood_pressure",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
			    success: function (res) {

			    	if (res.length > 0) {
				        for (var i=(res.length - 1); i>=0; i--) {
				        	znakiSisPovp+=res[i].systolic;
				        	znakiDisPovp+=res[i].diastolic;
				        	znaki.push([res[i].time.substring(0,10) , res[i].diastolic, res[i].systolic]);
				        }
				        znakiSisPovp=znakiSisPovp/res.length;
				        znakiDisPovp=znakiDisPovp/res.length;
			    	} 
			    	else {
			    		znaki.push(['Ni meritev', 0, 0]);
			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
			            "<span class='obvestilo label label-warning fade-in'>" +
			            "Ni podatkov!</span>");
			    	}
			    },
			    error: function() {
			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
		          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
		          JSON.parse(err.responseText).userMessage + "'!");
			    }
			});
			
		$.ajax({
  			    url: baseUrl + "/view/" + stalniEHR + "/" + "body_temperature",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
			    success: function (res) {
			    	znakiTemplength = res.length;
			    	if (res.length > 0) {
				        for (var i=(res.length - 1); i>=0; i--) {
				        	znakiTempPovp+=res[i].temperature;
				            znakiTemp.push([res[i].time.substring(0,10),res[i].temperature]);
			    		} 
			    		znakiTempPovp=znakiTempPovp/res.length;
			    	}
			    	else {
			    		znakiTemp.push(['Ni meritev', 0]);
			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
	                    "<span class='obvestilo label label-warning fade-in'>" +
	                    "Ni podatkov!</span>");
			    	}
			    },
			    error: function() {
			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			    }
			});
	}
}



var map;
var infowindow;


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        alert( "Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    x= position.coords.latitude;
    y= position.coords.longitude;
}

var x;
var y;

function initMap() {
	getLocation();
	setTimeout(function(){
	var pyrmont = {lat: x, lng: y};
		
		map = new google.maps.Map(document.getElementById('map'), {
			center: pyrmont,
			zoom: 10
		});

		infowindow = new google.maps.InfoWindow();
		var service = new google.maps.places.PlacesService(map);
		service.nearbySearch({
			location: pyrmont,
			radius: 40000,
			type: ['hospital']
	}, callback);
	}, 4000);
}

function callback(results, status) {
	if (status === google.maps.places.PlacesServiceStatus.OK) {
		for (var i = 0; i < results.length; i++) {
			createMarker(results[i]);
		}
	}
}

function createMarker(place) {
	var placeLoc = place.geometry.location;
	var marker = new google.maps.Marker({
		map: map,
		position: place.geometry.location
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent(place.name);
		infowindow.open(map, this);
	});
}

var sporocilo
function obvestila(){
	sporocilo= "";
	var barva = "#e0e0e0";
	
	if(BMI === -1){
		sporocilo= "Ni dovolj podatkov";
		barva = "#e0e0e0";
	}
	else if(BMI<16){
		sporocilo = "  Glede na vaš ITM spadate v kategorijo hude nedohranjenosti. Svetujemo vam posvet z osebnim zdravnikom.";
		barva = "#f98686";
	}
	else if(BMI<17){
		sporocilo = "  Glede na vaš ITM spadate v kategorijo zmerne nedohranjenosti. Svetujemo vam posvet z osebnim zdravnikom.";
		barva = "#f98686";
	}
	else if(BMI<18.5){
		if(BMIAktivnost==2)
			sporocilo = "  Glede na vaš ITM spadate v kategorijo blage nedohranjenosti. Skušajte pogosteje uživati obroke in za nekaj časa zmanjšati aktivnost.";
		else
			sporocilo = "  Glede na vaš ITM spadate v kategorijo blage nedohranjenosti. Skušajte pogosteje uživati obroke."
		barva = "#f7d980";
	}
	else if(BMI<25){
		if(BMIAktivnost==0)
			sporocilo = "  Glede na vaš ITM spadate v kategorijo normalne telesne mase. Vseeno poskušajte zvišati vašo aktivnost.";
		else
			sporocilo = "  Glede na vaš ITM spadate v kategorijo normalne telesne mase. Le tako naprej.";
		barva = "#c3f76f";
	}
	else if(BMI<30){
		if(BMIAktivnost==0)
			sporocilo = "  Glede na vaš ITM spadate v kategorijo zvečane telesne mase. Skušajte biti bolj aktivni in paziti na prehrano.";
		else
			sporocilo = "  Glede na vaš ITM spadate v kategorijo zvečane telesne mase. Skušajte bolj paziti na prehrano.";
		barva = "#aee6fc";
	}
	else if(BMI<35){
		if(BMIAktivnost==0)
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 1. Svetujemo vam povišanje telesne aktivnosti in uživanje zdrave prehrane.";
		else
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 1. Svetujemo vam uživanje bolj zdrave prehrane po več manjših obrokih.";
		barva = "#f7d980";
	}
	else if(BMI<40){
		if(BMIAktivnost==0)
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 2. Svetujemo vam, da se začnete bolj resno ukvarjati z aktivnostmi in predvsem paziti na prehrano.";
		else if(BMIAktivnost==1)	
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 2. Svetujemo vam uživanje bolj zdrave prehrane.";
		else if(BMIAktivnost==2)
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 2. Glede na vašo aktivnost je to zaskrbljujoče, zato vam svetujemo posvet z osebnim zdravnikom.";
		else
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 2. Svetujemo vam uživanje bolj zdrave prehrane in več gibanja.";
		barva = "#f98686";
	}
	else if(BMI>=40){
		if(BMIAktivnost==2)
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 3. Bodite še narej tako aktivni in poskušajte uživati zdravo prehrano.";
		else 
			sporocilo = "  Glede na vaš ITM spadate v kategorijo debelosti stopnje 3. Svetujemo vam posvet z osebnim zdravnikom";
		barva = "#f98686";
	}
	else{
		sporocilo = "Ni dovolj podatkov!";
	}
	//$("#obvestiloBMI").style.backgroundColor= barva;
	$("#obvestiloITM").empty();
	$("#obvestiloITM").append(" <div style='background-color: "+barva+"; padding: 5px; border-radius: 10px; font-size: 15'>"+sporocilo+"</div><br><div style='overflow: hidden;'><div id='slika1' style='cursor: pointer; float:left;' onclick='prikaziSliko1()'><img class='img-responsive' src='https://cdn0.iconfinder.com/data/icons/sports-android-l-lollipop-icon-pack/24/exercise-512.png' alt='activity' title='Klikni za več' style='width:240px;height:200px;'></div><div id='polje1' style='width:220px; height: 200px; float:left;display:none;padding:5px;font-size: 15px'>Telesna nedejavnost je že nekaj let na seznamu dejavnikov tveganja za nastanek številnih obolenj, med njimi zlasti bolezni srca in ožilja. Priporoča se vsaj 90 min telovadbe na teden, saj s tem narediš veliko dobrega za telo.</div><div id='slika2' style='cursor: pointer; float:left;' onclick='prikaziSliko2()'><img class='img-responsive' src='http://saveandrelief.org/wp-content/uploads/2014/05/Vegetables-Icon.png' alt='activity' title='Klikni za več' style='width:240px;height:200px;'></div><div id='polje2' style='width:220px; height:220px;display:none; float:left;padding:5px;font-size: 15px;'>Zdrava prehrana je zelo pomemben element vašega zdravja, zato je zelo pomembno, kakšno hrano jeste. Zdrava hrana je vir energije za vaše življenje tako kot čisto gorivo za vaš avto.</div></div>");
	BMI = 0;
	sporocilo="";
	
}

var klik1=0;
function prikaziSliko1(){
	if(klik1%2==0)
	$("#polje1").css({"display": "block"});
	else $("#polje1").css({"display": "none"});
	klik1++;
}
var klik2=0;
function prikaziSliko2(){
	if(klik2%2==0)
	$("#polje2").css({"display": "block"});
	else $("#polje2").css({"display": "none"});
	klik2++;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
	//document.getElementById("uporabniskoIme").value = jozeEHR;

	var joze=["Jože", "Hočevar",0, "1963", 0];
	var nika=["Nika", "Gradišek",1, "2000", 1];
	var borut=["Borut", "Petretič",0, "1992", 2];
	
	novEHRzaGenerirane(joze);
	novEHRzaGenerirane(nika);
	novEHRzaGenerirane(borut);
	
	setTimeout(function(){
		for(var i=0; i<10; i++){
			dodajVitalneMeritveGenerirano(0, jozeZnaki, i);
			dodajVitalneMeritveGenerirano(1, nikaZnaki, i);
			dodajVitalneMeritveGenerirano(2, borutZnaki, i);
		}
	}, 1200);

	
}

var jozeZnaki=[
		["2016-09-20", 170, 107.5, 37.0, 141, 88, 95],
		["2016-09-27", 170, 107.4, 38.1, 140, 82, 96],
		["2016-10-13", 170, 107.2, 39.1, 147, 85, 97],
		["2016-11-19", 170, 106.5, 39.7, 136, 74, 96],
		["2016-12-03", 170, 106.7, 36.5, 142, 78, 98],
		["2017-01-06", 170, 108.0, 38.0, 157, 89, 97],
		["2017-02-17", 170, 107.5, 36.9, 153, 93, 96],
		["2017-03-26", 170, 107.1, 37.1, 155, 85, 96],
		["2017-04-10", 170, 106.4, 38.0, 148, 83, 98],
		["2017-05-25", 170, 106.2, 36.0, 144, 80, 97],
		];

var nikaZnaki=[
		["2016-09-20", 165, 50.0, 35.2, 100, 51, 94],/////////
		["2016-09-27", 166, 50.4, 36.1, 100, 55, 96],
		["2016-10-13", 166, 50.2, 36.8, 103, 57, 97],
		["2016-11-19", 167, 50.5, 35.7, 101, 53, 96],
		["2016-12-03", 167, 51.7, 36.5, 97, 59, 95],
		["2017-01-06", 167, 52.0, 36.0, 99, 64, 97],
		["2017-02-17", 168, 51.5, 35.3, 102, 63, 95],
		["2017-03-26", 168, 50.1, 35.9, 111, 73, 96],
		["2017-04-10", 169, 51.4, 36.1, 108, 62, 97],
		["2017-05-25", 170, 52.2, 36.9, 104, 64, 97],/////////
		];

var borutZnaki=[
		["2016-09-20", 180, 75.1, 35.0, 115, 65, 95],/////////
		["2016-09-27", 180, 76.3, 35.1, 120, 67, 95],
		["2016-10-13", 180, 76.4, 36.2, 128, 66, 97],
		["2016-11-19", 180, 76.6, 36.3, 122, 74, 97],
		["2016-12-03", 180, 75.5, 36.2, 119, 72, 98],
		["2017-01-06", 181, 76.7, 35.7, 117, 70, 95],
		["2017-02-17", 181, 77.3, 37.4, 125, 73, 96],
		["2017-03-26", 181, 77.6, 36.9, 124, 78, 96],
		["2017-04-10", 181, 77.8, 36.6, 128, 69, 96],
		["2017-05-25", 181, 78.6, 37.0, 130, 80, 97],/////////
		];

var jozeEHR = "c2ebb953-0810-49e8-85cd-d072dd64447c";
var nikaEHR = "15019f50-2a5f-454c-b305-b860540b5e58";
var borutEHR = "0970112d-4f48-4175-b8d0-27b174650f9a";


function izberiOsebo(){
	var izbira = $("#izberiUporabnika").val();
	if(izbira == 0){
		document.getElementById("uporabniskoIme").value = jozeEHR;
	}
	else if(izbira == 1){
		document.getElementById("uporabniskoIme").value = nikaEHR;
	}
	else if(izbira == 2){
		document.getElementById("uporabniskoIme").value = borutEHR;
	}
	//document.getElementById("uporabniskoIme").value = izbira + "   " + borutEHR;
	
}


function novEHRzaGenerirane(ime){
	var sessionId = getSessionId();
	var EHR;
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
			if(ime[0]==="Jože")
				jozeEHR = ehrId;
			else if(ime[0]==="Nika")
				nikaEHR = ehrId;
			else if(ime[0]==="Borut")
				borutEHR = ehrId;
	        var partyData = {
	            firstNames: ime[0],
	            lastNames: ime[1],
	            dateOfBirth: ime[3],
	            partyAdditionalInfo: [{
	                key: "ehrId", value: ehrId },{
	                key: "activity", value: ime[4]},{
	                key: "genre", value: ime[2]
	            }]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	            		$('#izhodObvestilo').empty();
		                $("#izhodObvestilo").append("<div style='visibility:visible;background-color: #58b20e;color: white;text-align: center;border-radius: 50px; float: right; width: 50%; margin-top: 30px; height: 60px;padding-top: 15px;animation-name: exampleCorrect;animation-duration: 1s;'>Uspešno generirani 3 uporabniki</div>");

		                
	            },
	            error: function(err) {
	            }
	        });
	    }
	});
}


function dodajVitalneMeritveGenerirano(oseba, ime, i) {
		sessionId = getSessionId();

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
    		// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": ime[i][0],
		    "vital_signs/height_length/any_event/body_height_length": ime[i][1],
		    "vital_signs/body_weight/any_event/body_weight": ime[i][2],
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": ime[i][3],
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": ime[i][4],
		    "vital_signs/blood_pressure/any_event/diastolic": ime[i][5],
		    "vital_signs/indirect_oximetry:0/spo2|numerator": ime[i][6]
		};
		if(oseba==0){
		var parametriZahteve = {
		    ehrId: jozeEHR,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "SAM"
		};
		}
		else if(oseba==1){
		var parametriZahteve = {
		    ehrId: nikaEHR,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "SAM"
		};
		}
		else if(oseba==2){
		var parametriZahteve = {
		    ehrId: borutEHR,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "SAM"
		};
		}
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {},
		    error: function(err) {
		    }
		});
}



